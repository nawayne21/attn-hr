from django.db import models

# Create your models here.
class Employee(models.Model):
	# empNo = models.IntegerField(primary_key=True)
	first_name = models.CharField(max_length=20)
	last_name = models.CharField(max_length=20)
	gender = models.CharField(max_length=1)
	hire_date = models.DateField()

	def __str__(self):
		return "{} {}".format(self.first_name, self.last_name)

class InOut(models.Model):
	CHECK_TYPE = (
        ('I', 'In'),
        ('O', 'Out'),
    )
	user_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
	check_date = models.DateField()
	check_time = models.TimeField()
	check_type = models.CharField(max_length=1, choices=CHECK_TYPE)

	def __str__(self):
		return "{} : {} - {}".format(self.user_id, self.check_date, self.check_type)