from django.shortcuts import get_object_or_404, render

from .models import Employee, InOut

# Create your views here.

def index(request):
	employee_list = Employee.objects.order_by('-hire_date')[:5]
	context = {'employee_list': employee_list}
	return render(request, 'attendance/index.html', context)

def details(request, employee_id):
	employees = get_object_or_404(Employee, id=employee_id)
	inouts = InOut.objects.filter(user_id = employee_id)

	context = {'employees' : employees,
	'inouts' : inouts
	}
	return render(request, 'attendance/details.html', context)